package com.frx01.article.service;

import com.frx01.article.dao.CommentRepository;
import com.frx01.article.pojo.Comment;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2023年11月25日 10:55
 */
@Service
public class CommentService  {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 保存一个评论
     * @param comment
     */
    public void save(Comment comment){
        commentRepository.save(comment);
    }

    /**
     * updateByCommentId 直接调用save方法
     * @param comment
     */
    public void updateByCommentId(Comment comment){
        if(comment==null || comment.getId() == null){
            System.out.println("本次更新失败");
            return;
        }
        Optional<Comment> result = commentRepository.findById(comment.getId());
        Comment oldComment = result.get();
        String articleid = comment.getArticleid();
        String content = comment.getContent();
        Integer likenum = comment.getLikenum();
        String nickname = comment.getNickname();
        Integer replynum = comment.getReplynum();
        String state = comment.getState();
        String userid = comment.getUserid();
        if(Strings.isNotBlank(articleid)) oldComment.setArticleid(articleid);
        if(Strings.isNotBlank(content)) oldComment.setContent(content);
        if(replynum!=null) oldComment.setReplynum(replynum);
        if(likenum!=null) oldComment.setLikenum(likenum);
        if(Strings.isNotBlank(nickname)) oldComment.setNickname(nickname);
        if(Strings.isNotBlank(state)) oldComment.setState(state);
        if(Strings.isNotBlank(userid)) oldComment.setUserid(userid);
        this.save(oldComment);
    }

    /**
     * 通过CommentId删除评论
     * @param id
     */
    public void deleteByCommentId(String id){
        commentRepository.deleteById(id);
    }

    /**
     * 查询所有评论
     * @return
     */
    public List<Comment> findAll(){
        return commentRepository.findAll();
    }

    /**
     * 通过CommentId查询一条评论
     * @param id
     * @return
     */
    public Comment findCommentById(String id){
        //调用dao
        return commentRepository.findById(id).get();
    }

    /**
     * 根据parentid查询分页列表
     * @param parentid
     * @param page
     * @param size
     * @return
     */
    public Page<Comment> findCommentListByParentid(String parentid, int page, int size){
        return commentRepository.findByParentid(parentid, PageRequest.of(page-1,size));
    }

    /**
     * 通过Id让评论数+1
     * @param id
     */
    public void updateCommentLikeNum(String id){
        // 查询条件
        Query query = Query.query(Criteria.where("_id").is(id));

        // 更新条件
        Update update = new Update();

        //局部更新,相当于$set
        update.inc("likenum");

        // 参数1:查询对象
        // 参数2:更新的内容
        // 参数3:集合的名字或实体类的类型
        mongoTemplate.updateFirst(query,update,Comment.class);
    }

}
