package com.frx01.article;

import com.frx01.article.pojo.Comment;
import com.frx01.article.service.CommentService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
class ArticleApplicationTests {

    @Autowired
    private CommentService commentService;

    @Test
    void contextLoads() {
    }

    @Test
    public void TestFindCommentById(){
        Comment result = commentService.findCommentById("2");
        System.out.println(result);
    }

    /**
     * 测试获取所有的Comment
     */
    @Test
    public void TestFindAll(){
        List<Comment> results = commentService.findAll();
        for (Comment result : results) {
            System.out.println(result);
        }
    }

    /**
     * 测试添加一条评论
     */
    @Test
    public void TestSaveComment(){
        Comment comment = new Comment();
        comment.setId("6");
        comment.setArticleid("10086");
        comment.setContent("怕上火，喝加多宝");
        comment.setLikenum(120);
        comment.setCreatedatetime(LocalDateTime.now());
        comment.setArticleid("2");
        comment.setReplynum(5);
        comment.setUserid("10086");
        comment.setState("1");
        comment.setNickname("tkx");
        comment.setParentid("3");
        commentService.save(comment);
    }


    /**
     * 根据CommentId删除评论
     */
    @Test
    public void deleteByCommentId(){
        commentService.deleteByCommentId("4");
        System.out.println("删除成功");
    }

    @Test
    public void testFindCommentListByParentId(){
        Page<Comment> pageResponse = commentService.findCommentListByParentid("3", 2, 2);
        System.out.println("----总记录条数---"+pageResponse.getTotalElements());
        System.out.println("-----当前页数据-------:"+pageResponse.getContent());
    }


    /**
     * 更新一个评论的内容
     */
    @Test
    public void testUpdate(){
        Comment comment = new Comment();
        comment.setId("3");
        comment.setArticleid("10086");
        commentService.updateByCommentId(comment);
    }


    @Test
    public void testupdataCommentLikenum(){
        String id = "2";
        Integer likenum = commentService.findCommentById(id).getLikenum();
        System.out.println("更新前："+likenum);
        commentService.updateCommentLikeNum("2");
        likenum = commentService.findCommentById(id).getLikenum();
        System.out.println("更新后："+likenum);
    }
}
